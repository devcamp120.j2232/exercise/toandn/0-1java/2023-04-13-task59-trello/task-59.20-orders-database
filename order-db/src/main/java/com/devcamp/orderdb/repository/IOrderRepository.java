package com.devcamp.orderdb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderdb.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {

}
