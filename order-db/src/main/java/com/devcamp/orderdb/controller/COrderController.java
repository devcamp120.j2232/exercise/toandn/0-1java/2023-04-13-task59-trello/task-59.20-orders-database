package com.devcamp.orderdb.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderdb.model.COrder;
import com.devcamp.orderdb.repository.IOrderRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class COrderController {
  @Autowired
  IOrderRepository iOrderRepository;

  @GetMapping("/orders")
  public ResponseEntity<List<COrder>> getDrinks() {
    try {
      List<COrder> orderList = new ArrayList<COrder>();
      iOrderRepository.findAll().forEach(orderList::add);
      if (orderList.size() == 0) {
        return new ResponseEntity<>(orderList, HttpStatus.NOT_FOUND);
      } else
        return new ResponseEntity<>(orderList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
